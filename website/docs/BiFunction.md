Represents a function that accepts two arguments and produces a result.

Declaration:-

Interface BiFunction<T,U,R>


Here, 

T - the type of the first argument as input
U - the type of the second argument as input
R - the type of the result of the function

```java
Example:-

package com.test;

import java.util.function.BiFunction;
import java.util.function.Function;

public class BiFunctionExample {
 
         public static void main(String[] args) {
  

                 //Function example
                Function<Integer, Integer> printNumber = a -> a*10;
                System.out.println("Number is "+printNumber.apply(10));
  
                 //BiFunction example

                BiFunction<Integer, Integer, Integer> add = (a, b) -> a+b;
                System.out.println("Summation of two number is "+add.apply(3,2));
        }

}
```
