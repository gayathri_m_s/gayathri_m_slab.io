- BiConsumer is a functional interface, has one abstract method i.e accept(), similar to a Consumer Interface. 

- The difference between consumer and BiConsumer is Consumer interface accepts single parameter where as BiConsumer accepts two input parameters, and both doesn’t return anything.

```java


package com.test;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class BiConsumerExample {
 
         public static void main(String[] args) {
      

                   //Consumer example
                  Consumer<String> strConsumer = str -> System.out.println(str);
                  strConsumer.accept("Java Wolrd");
  
                  //BiConsumer example
                  Map<String, String> map = new HashMap<String, String>();
                  map.put("Hello", "World");
                  BiConsumer<String, String> consumer = (key, value) -> System.out.println("Key - "+ key+", Value - "+value);
    
                  map.forEach(consumer);
         }

}

```
