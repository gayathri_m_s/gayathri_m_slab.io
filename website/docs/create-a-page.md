---
title: Stream API
---

## What is Stream?

- Java streams represent a pipeline through which the data will flow and the functions to operate on the data.
-  You can specify the functions that operate on that data.
- The primary distinction between a stream and a storage structure is that a stream does not store data.
- A stream is not a collection of elements that can be stored. For example, you cannot point to a location in the stream where a certain element exists.
- Streams once consumed cannot be reused


```java
   // data set
List<Employee> employees = Arrays.asList(
        new Employee("ananth", 28),
        new Employee("raman", 27),
        new Employee("suraj", 25),
        new Employee("shwetha", 24)
        //employee.stream()
);

```

## Map

- Map transforms one value to another value.
- Map allows you to perform a computation on the data inside a stream.
  
```java
// Using traditional approach
List<String> names = new ArrayList<>();
for (Employee employee : employees) {
    names.add(employee.name());
}
// Simplified approach using map()
names = employees.stream().map(Employee::name).collect(Collectors.toList());
```

## Filter

- It allows to filter stream elements on the basis of given predicate. 

```java
// Traditional approach
for (Employee employee : employees) {
    if (employee.age() > 25) {
        System.out.println(employee);
    }
}
// Simplified approach using filter()
employees.stream().filter(employee -> employee.age() > 25).forEach(System.out::println);

```

## Count

- It returns the count of elements in this stream.

```java
// Traditional approach
long count = 0;
for (Employee employee : employees) {
    if (employee.age() > 25) {
        count++;
    }
}
// Simplified approach using count()
count = employees.stream().filter(employee -> employee.age() > 25).count();
```

## Collect

- Collect takes a Collector as parameter and is tailored to create, mutate, merge and optionally makes unmodifiable any mutable collections.
- You can create your own collector by implementing the [Collector interface](https://docs.oracle.com/javase/8/docs/api/java/util/stream/Collector.html)

```java
// Traditional approach
names = new ArrayList<>();
for (Employee employee : employees) {
    names.add(employee.name());
}
// Simplified approach using collect()
names = employees.stream().map(Employee::name).collect(Collectors.toList());
```

## forEach

- Performs the given action for each element of the stream

```java

// Traditional approach
for (Employee employee : employees) {
    if (employee.age() > 25) {
        System.out.println(employee);
    }
}
// Simplified approach using forEach()
employees.stream().filter(employee -> employee.age() > 25).forEach(System.out::println);
```

## findFirst

- The findFirst method returns the first element in a Stream wrapped in **Optional**  (a container object which may or may not contain a non-null value) 
- If the stream has no encounter order, then any element may be returned.
  
```java
  // Traditional approach
for (Employee employee : employees) {
    if (employee.age() > 25) {
        System.out.println(employee);
        break;
    }
}
// Simplified approach using findFirst()
employees.stream().filter(employee -> employee.age() > 25).findFirst().ifPresent(System.out::println);
```
