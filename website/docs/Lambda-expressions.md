---
title: Lambda expressions
slug: /
---
## What is Lambda expression?
- A short block of code which takes in parameters and returns a value. 
- It is similar to methods, but they do not need a name and they can be implemented right in the body of a method.
- The functional interface is used to implement lambda expression and it referes to an interface with only one abstract method. 
- Annotation [@FunctionalInterface](https://docs.oracle.com/javase/8/docs/api/java/lang/FunctionalInterface.html) can be used to declare an interface as a functional interface.

## Syntax 

Lambda expressions consist of three entities:

![Lambda syntax](/img/lambda.drawio.png)

## Example
```java
//Without Lambda expression
  new Thread(new Runnable()
  {
   @Override
   public void run()
   {
    // Do-Something
   }
  }).start();

//With Lambda expression
  new Thread(() -> { /* Do something*/} ).start(); 
```




